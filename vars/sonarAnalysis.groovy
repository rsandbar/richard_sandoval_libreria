def call(boolean abortPipeline = false, boolean qualityGateOk = false, String rama = '') {
    timeout(time: 5, unit: 'MINUTES') {
        // Realizar escaneo de SonarQube o imprimir mensaje de prueba 
        echo "Ejecución de las pruebas de calidad de código"

        // Evaluar el QualityGate de SonarQube y decidir si se aborta el pipeline
        if (qualityGateOk) {
            if (!qualityGatePasses()) {
                error "El QualityGate de SonarQube no ha sido aprobado. Abortando el pipeline."
            }
        }
    }

    // Abortar el pipeline si abortPipeline = true
    if (abortPipeline) {
        error "Se aborta el pipeline."
    }
    else {
        switch(rama) {
            case 'master':
                error "Proviene de rama master. Se corta el pipeline.";
            break
            case 'hotfix':
                error "Proviene de rama hotfix. Se corta el pipeline.";
            break
        }
    }
}
